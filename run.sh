#!/bin/bash
file="$1"
n=1

IFS="." read -a table <<< "$1"

echo "CREATE TABLE ${table[0]} (id LONG PRIMARY KEY, metricId INTEGER, timestamp LONG, value INTEGER)" |  bin/sqlline.sh --color=true --verbose=true -u jdbc:ignite:thin://127.0.0.1/


while read line
do

IFS="," read -a tmp <<< "$line"

echo "INSERT INTO ${table[0]} (id, metricId, timestamp, value) VALUES ($n, ${tmp[0]}, ${tmp[1]}, ${tmp[2]})" |  bin/sqlline.sh --color=true --verbose=true -u jdbc:ignite:thin://127.0.0.1/
((n++))

done < $file
