#!/usr/bin/python
import time
import numpy
import random
import sys
import argparse

#todo:
# allow writing different patterns (Common Log, Apache Error log etc)
# log rotation


class switch(object):
    def __init__(self, value):
        self.value = value
        self.fall = False

    def __iter__(self):
        """Return the match method once, then stop"""
        yield self.match
        raise StopIteration

    def match(self, *args):
        """Indicate whether or not to enter a case suite"""
        if self.fall or not args:
            return True
        elif self.value in args: # changed for v1.5, see below
            self.fall = True
            return True
        else:
            return False

parser = argparse.ArgumentParser(__file__, description="Fake Apache Log Generator")
parser.add_argument("--output", "-o", dest='output_type', help="Write to a Log file, a gzip file or to STDOUT", choices=['LOG','GZ','CONSOLE'] )
parser.add_argument("--num", "-n", dest='num_lines', help="Number of lines to generate (0 for infinite)", type=int, default=1)

args = parser.parse_args()

log_lines = args.num_lines
output_type = args.output_type
timestr = time.strftime("%Y%m%d-%H%M%S")

outFileName = 'INPUTLINES.log'

for case in switch(output_type):
	if case('LOG'):
		f = open(outFileName,'w')
		break
	if case():
		f = sys.stdout

response=["1","2","3","4"]
countN = 0
flag = True
while (flag):
	tp = int(time.time()) + countN*1000
	countN+=1
	resp = numpy.random.choice(response,p=[0.2,0.3,0.4,0.1])
	value = random.randint(10, 100)

	f.write('%s,%s,%s\n' % (resp,tp,value))
	f.flush()

	log_lines = log_lines - 1
	flag = False if log_lines == 0 else True

