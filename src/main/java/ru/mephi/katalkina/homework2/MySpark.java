package ru.mephi.katalkina.homework2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import scala.Tuple2;

public class MySpark {
	
	protected static final String SCALE = "3m";
	protected static final long scale = 180000;
	protected static final long time = 1512931745;
	
	/**
	 * key statistics
	 */
	public static class Stats implements Serializable {

		private static final long serialVersionUID = 1L;
		private final int numBytes;
		private final int count;

		// constructor
		public Stats(int numBytes, int count) {
			this.numBytes = numBytes;
			this.count = count;
		}
		
		// sum of bytes and count for calculating the average value
		public Stats merge(Stats other) {
			return new Stats(numBytes + other.numBytes, count + other.count);
		}

		public String toString() {
			return String.format("%s", numBytes/count);
		}
	}
	
	/**
	 * sort by key output data
	 */
	static class TupleComparator implements Comparator<Tuple2<Integer, Integer>>, Serializable {
		
		@Override
		public int compare(Tuple2<Integer, Integer> v1, Tuple2<Integer, Integer> v2) {
			
			if(v1._1().compareTo(v2._1()) != 0){
				return v1._1().compareTo(v2._1());
			} else {
				return v1._2().compareTo(v2._2());
			}        
		}
	}
	
	/**
	 * get data from Ignite
	 * compute data
	 * 
	 * @param args 
	 */
	public static void main(String[] args) {
		
		// init Spark conf
		SparkConf sparkConf = new SparkConf().setAppName("Count");
		sparkConf.setMaster("local");
	 
		// init Spark context
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);	
	 
		MyIgnite ign = new MyIgnite();

		// list from Ignite
		ArrayList<String> list =  ign.readData(args[0]);
		
		// convert string lines to javaRDD
		JavaRDD<String> dataSet = sparkContext.parallelize(list);
		
		// convert javaRDD to javaPairRDD
		JavaPairRDD<Tuple2<Integer, Integer>, Stats> lines = dataSet.mapToPair(s -> new Tuple2<>(extractKey(s.split(",")),ectractValue(s.split(","))));
		JavaPairRDD<Tuple2<Integer, Integer>, Stats> sortedlines = lines.reduceByKey(Stats::merge).sortByKey(new TupleComparator(), true);

		// reduce by key (metricID, timestamp)
		List<Tuple2<Tuple2<Integer, Integer>, Stats>> counts = sortedlines.collect();

		// close Spark context
		sparkContext.close();

		// print result
		for(Tuple2<Tuple2<Integer, Integer>, Stats> t : counts) {
			long n = (t._1()._2() - 1) * scale + time;
			System.out.println(t._1()._1() + "," + n + "," + SCALE + "," + t._2());
		}
	}
	
	/**
	 * key to javaPairRDD
	 * 
	 * @param String[] s - split line
	 * @return Tuple2<Integer,Integer>
	 */
	protected static Tuple2<Integer, Integer> extractKey(String[] s) {
		long times = Long.parseLong(s[1]);
		int n = (int) ((times-time)/scale) + 1;
		return new Tuple2<Integer,Integer>(Integer.parseInt(s[0]),n);
	}

	/**
	 * value to javaPairRDD
	 * 
	 * @param String[] s - split line
	 * @return Stats(numBytes,count)
	 */
	protected static Stats ectractValue(String[] s) {
		return new Stats(Integer.parseInt(s[2]), 1);
	}
}