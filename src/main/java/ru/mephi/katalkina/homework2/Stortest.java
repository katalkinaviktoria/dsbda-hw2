package ru.mephi.katalkina.homework2;

import java.io.Serializable;
import org.apache.ignite.cache.query.annotations.QuerySqlField;

/**
 * table properties
 */
public class Stortest implements Serializable{

	private static final long serialVersionUID = 1L;

	@QuerySqlField(index = true)
	private long id;

	@QuerySqlField
	private Integer metricId;
	
	@QuerySqlField
	private long timestamp;
	
	@QuerySqlField
	private Integer value;
}
