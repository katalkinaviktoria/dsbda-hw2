package ru.mephi.katalkina.homework2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.SqlFieldsQuery;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;

public class MyIgnite {
	
	/**
	 * get data from Ignite Native Persistence                        
	 * read lines from table 
	 * 
	 * @param dbName - name of table
	 * @return ArrayList<String> - data from table
	 */
	ArrayList<String> readData(String dbName) {

		// create a new instance of TCP Discovery SPI
		TcpDiscoverySpi spi = new TcpDiscoverySpi();
        
		// create a new instance of tcp discovery multicast ip finder
		TcpDiscoveryMulticastIpFinder tcMp = new TcpDiscoveryMulticastIpFinder();
        
		// change your IP address here
		tcMp.setAddresses(Arrays.asList("localhost")); 
        
		// set the multi cast ip finder for spi
		spi.setIpFinder(tcMp);

		// create new ignite configuration
		IgniteConfiguration cfg = new IgniteConfiguration();
		cfg.setClientMode(true);
        
		// set the discovery§ spi to ignite configuration
		cfg.setDiscoverySpi(spi);

		// start ignite
		Ignite ignite = Ignition.start(cfg);
		ignite.active(true);

		// get or create cache
		IgniteCache<Long, Stortest> cache = ignite.getOrCreateCache("SQL_PUBLIC_"+ dbName);
        
		// query
		String sql = "select metricID, timestamp, value from " + dbName;

		// conversion of query results
		QueryCursor<List<?>> res = cache.query(new SqlFieldsQuery(sql));
		ArrayList<String> list = new ArrayList<String>();
		for (List<?> row : res) {
			list.add(row.get(0) + "," + row.get(1) + "," + row.get(2));
		}
        
		// close ignite
		ignite.close();
        
		return list;
	}
}
