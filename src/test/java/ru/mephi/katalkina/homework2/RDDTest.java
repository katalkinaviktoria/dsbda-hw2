package ru.mephi.katalkina.homework2;

import static org.junit.Assert.assertEquals;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.junit.Test;

import com.holdenkarau.spark.testing.SharedJavaSparkContext;

import ru.mephi.katalkina.homework2.MySpark.Stats;
import scala.Tuple2;

public class RDDTest extends SharedJavaSparkContext implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * create input data
	 * convert to javaRDD
	 * convert to javaPairRDD
	 * check compute
	 */
	@Test
	public void testSort() {
		ArrayList<String> input = new ArrayList<String>();
		input.add("1,1512931745,78");
		input.add("1,1512931745,78");
		input.add("1,1512931745,78");
		JavaRDD<String> dataSet = jsc().parallelize(input);
		JavaPairRDD<Tuple2<Integer, Integer>, Stats> lines = dataSet.mapToPair(s -> new Tuple2<>(MySpark.extractKey(s.split(",")), MySpark.ectractValue(s.split(","))));
		List<Tuple2<Tuple2<Integer, Integer>, Stats>> counts = lines.reduceByKey(Stats::merge).collect();
		Tuple2<Tuple2<Integer, Integer>, Stats> t = counts.get(0);
		assertEquals(t._1()._1() + "," + (MySpark.time + (t._1()._2() - 1) * MySpark.scale) + "," + MySpark.SCALE + "," + t._2(), "1,1512931745,3m,78");
	}
}
