package ru.mephi.katalkina.homework2;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class IgniteTest {
	
	/**
	 * init MyIgnite
	 * check connection Ignite
	 * check data is not null
	 */
	@Test
	public void test() {
		MyIgnite ign = new MyIgnite();
		assertNotNull(ign.readData("SIXTESTS"));
	}
}
