# HOMEWORK 2 #

DS:BDA HW2 repo

### REQUIREMENTS

- Operating system: Ubuntu 16.04
- Apache Ignite 2.3.0
- Apache Spark 2.2.0
- Scala 2.11.0
- RAM: 4+ GiB

### MANUAL

Build: 
```sh
$ git clone git@bitbucket.org:katalkinaviktoria/dsbda-hw2.git
$ cd dsbda-hw2/
$ mvn clean install
```
Prepare input data:
```sh
$ bin/ignite.sh config.xml
$ python fake-log.py -n 1000 -o LOG
$ chmod +x run.sh
$ ./run.sh INPUTLINES.log
```
Run:
```sh
$ bin/spark-submit --class ru.mephi.katalkina.homework2.MySpark --master local 
	--deploy-mode client --executor-memory 1g 
	target/homework2-0.0.1-SNAPSHOT-jar-with-dependencies.jar INPUTLINES
```